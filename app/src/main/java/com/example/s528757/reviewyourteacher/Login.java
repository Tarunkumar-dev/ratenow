package com.example.s528757.reviewyourteacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class Login extends AppCompatActivity {
    TextView tv,tv2;
    Button btn;
    EditText usernameField,passwordField;
    public static final String APPLICATION_ID = "8F3B3EEE-AA4D-7C8C-FF14-8C873B963600";
    public static final String API_KEY = "5E116BCC-66AB-D04E-FF7D-63E455CDD600";
    public static final String SERVER_URL = "https://api.backendless.com";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Backendless.initApp(this,Defaults.APPLICATION_ID,API_KEY);
        tv= (TextView) findViewById(R.id.textView);
        btn= (Button) findViewById(R.id.button);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);

        //link to redirect to sign up activity
        tv.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v){
                        Intent intent = new Intent(getApplicationContext(), Signup.class);
                        startActivity(intent);
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameField=(EditText) findViewById(R.id.username);
                passwordField=(EditText) findViewById(R.id.password);

               final String username = usernameField.getText().toString();
               final String password = passwordField.getText().toString();

                Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        System.out.println(response);
                        BackendlessUser user = Backendless.UserService.CurrentUser();
                        String role = (String) user.getProperty( "role" );
                        if(role.equals("admin")){
                            Intent intent = new Intent(getApplicationContext(), adminActivity.class);
                            Toast.makeText(getApplicationContext(), "Please wait, logging in as admin", Toast.LENGTH_LONG).show();
                            startActivity(intent);
                        }
                        else if(role.equals("student")){
                            Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
                            Toast.makeText(getApplicationContext(), "Please wait, logging in as student", Toast.LENGTH_LONG).show();
                          startActivity(intent);
                        }
                        else if(role.equals("faculty")){
                            Intent intent = new Intent(getApplicationContext(), facultyNotifications.class);
                            Toast.makeText(getApplicationContext(), "Please wait, logging in as faculty", Toast.LENGTH_LONG).show();
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Invalid user", Toast.LENGTH_SHORT).show();
                        }
//                        if ((username.equals("admin"))
//                                && password.equals("password"))
//                        {
//                            Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
//
//                            Intent intent = new Intent(getApplicationContext(), adminActivity.class);
//                            startActivity(intent);
//                        }
//                        else if(username.equals("student")&& password.equals("password"))
//                        {
//                            Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
//
//                            Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
//                            startActivity(intent);
//                        }
//                        else if(username.equals("faculty")&& password.equals("password"))
//                        {
//                            Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
//
//                            Intent intent = new Intent(getApplicationContext(), facultyNotifications.class);
//                            startActivity(intent);
//                        }


                    }

                    @Override
                    public void handleFault(BackendlessFault fault)
                    {

                        Toast.makeText(getApplicationContext(), "Wrong Credentials",Toast.LENGTH_SHORT).show();
                        usernameField.setText("");
                        passwordField.setText("");
//                            et.setVisibility(View.VISIBLE);
//                    et2.setBackgroundColor(Color.RED);




                        System.out.println(fault);
                    }
                });







            }
        });
//        btn.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Intent intent = new Intent(getApplicationContext(), studentActivity.class);
//                startActivity(intent);
//            }
//        });


    }
}

package com.example.s528757.reviewyourteacher;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by s528757 on 10/27/2017.
 */

public class FacultyCoursesLVAdapter extends ArrayAdapter {
    public FacultyCoursesLVAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull ArrayList<FacultyCourseSelection> objects)
    {
        super(context, resource, textViewResourceId, objects);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        TextView tv1 = v.findViewById(R.id.Course);
//        TextView tv2 = v.findViewById(R.id.Professor);
        TextView tv3 = v.findViewById(R.id.Section);
        FacultyCourseSelection SCS = (FacultyCourseSelection) getItem(position);
        tv1.setText(SCS.getCourseName());
//        tv2.setText(SCS.getProfName());
        tv3.setText(SCS.getSecNumber());

        return v;




    }

}
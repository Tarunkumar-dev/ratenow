package com.example.s528757.reviewyourteacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.HashMap;
import java.util.Map;

public class Signup extends AppCompatActivity {


HashMap User = new HashMap();
    Button btn;
    EditText emailField, usernameField, passwordField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);
        //Backendless.setUrl( Defaults.SERVER_URL );

        //Backendless.initApp(getApplicationContext(),Defaults.APPLICATION_ID, "cat", "4.3.9" );

        emailField =(EditText) findViewById(R.id.email);
        usernameField = (EditText) findViewById(R.id.username);
        passwordField = (EditText) findViewById(R.id.password);
        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();
                String email = emailField.getText().toString();

//                User.put("username",username);
//                User.put("Password",password);
//                User.put("Email",email);
                //Creating a new user
                BackendlessUser users = new BackendlessUser();
                users.setPassword(password);
                users.setEmail(email);
                users.setProperty("name", username);
                Backendless.UserService.register(users, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getApplicationContext(),"Successfull", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), Login.class);
                        startActivity(intent);
                        Log.d("Signup succesfull : ","Signup succesfull data is sent to backendless DB ");
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getApplicationContext(), " Unsuccessful", Toast.LENGTH_LONG).show();

                    }
                });

//                Backendless.Persistence.of( "User" ).save( User, new AsyncCallback<Map>() {
//                    public void handleResponse( Map response )
//                    {
//
//                        Toast.makeText(getApplicationContext(),"Registration Successfull", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(getApplicationContext(), Login.class);
//                        startActivity(intent);
//                        Log.d("Signup succesfull : ","Signup succesfull data is sent to backendless DB ");
//                    }
//
//                    public void handleFault( BackendlessFault fault )
//                    {
//                        Toast.makeText(getApplicationContext(), "Registration Unsuccessful", Toast.LENGTH_LONG).show();
//
//                    }
//                });



            }
        });


    }
}

package com.example.s528757.reviewyourteacher;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class studentCourses extends Activity implements View.OnClickListener {




    Button add;
    String course, professor, section;
    HashMap courses = new HashMap();





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
                startActivity(intent);

                Toast.makeText(studentCourses.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), studentCourses.class);
                startActivity(intent1);
                Toast.makeText(studentCourses.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                Toast.makeText(studentCourses.this,"logged out",Toast.LENGTH_LONG).show();

        }
        return super.onOptionsItemSelected(item);
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_courses);
        add = (Button)findViewById(R.id.ADD);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText et = (EditText) findViewById(R.id.ETCourse);
                course = et.getText().toString();
                final EditText et2 = (EditText) findViewById(R.id.ETProf);
                professor = et2.getText().toString();
                final EditText et3 = (EditText) findViewById(R.id.ETSec);
                section = et3.getText().toString();

                courses.put("course", course);
                courses.put("professor", professor);
                courses.put("section", section);

                Backendless.Persistence.of("Student_Courses").save(courses, new AsyncCallback<Map>() {
                    @Override
                    public void handleResponse(Map response) {
                        Toast.makeText(getApplicationContext(), "Course added", Toast.LENGTH_SHORT).show();
                        et.setText("");
                        et2.setText("");
                        et3.setText("");

                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {

                    }
//

                });
            }
        });



    }

    @Override
    public void onClick(View view) {

        insertData(course, professor, section);


    }

    private void insertData(String course, String professor, String section) {
        // TODO Auto-generated method stub
//        DBHelper helper = new DBHelper(getApplicationContext(),1);
//        SQLiteDatabase database = helper.getWritableDatabase();
//        ContentValues content = new ContentValues();
//        content.put(DBHelper.COLUMN_COURSE, course);
//        content.put(DBHelper.COLUMN_PROFESSOR, professor);
//        content.put(DBHelper.COLUMN_SECTION, section);
//
//        database.insert(DBHelper.TABLE_NAME, null, content);
    }

    public void submit(View v)
    {
        Intent intent = new Intent(getApplicationContext(), StudentReviewActivity.class);
        startActivity(intent);
    }



//    public void newCourse(View v)
//    {
//        EditText et = (EditText) findViewById(R.id.ETCourse);
//        course = et.getText().toString();
//        EditText et2 = (EditText) findViewById(R.id.ETProf);
//        professor = et2.getText().toString();
//        EditText et3 = (EditText) findViewById(R.id.ETSec);
//        section = et3.getText().toString();
//        insertItem(course,professor,section);
//
////        final ArrayAdapter<StudentCourseSelction> adapter = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Course, data);
////        final ListView list = (ListView) findViewById(R.id.sCourseLV);
////        EditText et = (EditText) findViewById(R.id.ETCourse);
////        course = et.getText().toString();
////
////
////        final ArrayAdapter<StudentCourseSelction> adapter2 = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Professor, data);
////        final ListView list2 = (ListView) findViewById(R.id.sCourseLV);
////        EditText et2 = (EditText) findViewById(R.id.ETProf);
////        professor = et2.getText().toString();
////
////
////        final ArrayAdapter<StudentCourseSelction> adapter3 = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Section, data);
////        final ListView list3 = (ListView) findViewById(R.id.sCourseLV);
////        EditText et3 = (EditText) findViewById(R.id.ETSec);
////        section = et3.getText().toString();
////
////        data.add(new StudentCourseSelction(course,professor,section));
////        Toast.makeText(getApplicationContext(), "New Course Is Added", Toast.LENGTH_LONG).show();
////        list.setAdapter(adapter);
////        list2.setAdapter(adapter2);
////        list3.setAdapter(adapter3);
////    final ArrayAdapter<StudentCourseSelction> adapter = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Coursenames, data);
////    final ListView list = (ListView) findViewById(R.id.sCourseLV);
////
////
////    Spinner sp1 = (Spinner) findViewById(R.id.Coursenames);
////    String value = String.valueOf(sp1.getSelectedItem());
////
////    final ArrayAdapter<StudentCourseSelction> adapter2 = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Profnames, data);
////    final ListView list2 = (ListView) findViewById(R.id.sCourseLV);
////    Spinner sp2 = (Spinner) findViewById(R.id.Profnames);
////    String value2 = String.valueOf(sp2.getSelectedItem());
////
////
////    final ArrayAdapter<StudentCourseSelction> adapter3 = new StudentCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Secnumbers, data);
////    final ListView list3 = (ListView) findViewById(R.id.sCourseLV);
////    Spinner sp3 = (Spinner) findViewById(R.id.Secnumbers);
////    String value3 = String.valueOf(sp3.getSelectedItem());
//
//
////    sp1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////
////        String course;
////        @Override
////        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////            switch (i){
////                case 0: course ="GDP";
////                break;
////                case 1: course ="HCI";
////                    break;
////                case 2: course ="webapps";
////                    break;
////                case 3: course ="oops";
////                    break;
////                case 4: course ="Android";
////                    break;
////                case 5: course ="PM";
////                    break;
////                case 6: course ="ITM";
////                    break;
////                case 7: course ="ADB";
////                    break;
////                case 8: course ="NS";
////                    break;
////                default:course="";
////
////            }
////        }
////    });
////
////    sp2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////
////        String Faculty;
////        @Override
////        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////            switch (i){
////                case 0: Faculty ="Qin";
////                    break;
////                case 1: Faculty ="Badami";
////                    break;
////                case 2: Faculty ="dougles hawley";
////                    break;
////                case 3: Faculty ="Michel Roggers";
////                    break;
////                case 4: Faculty ="Michel Oudshourn";
////                    break;
////                case 5: Faculty ="Charls Hoot";
////                    break;
////                case 6: Faculty ="Charitha";
////                    break;
////                case 7: Faculty="Ankith choudary";
////                    break;
////                default:Faculty="";
////
////            }
////        }
////    });
////
////    sp3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////
////        int i;
////        @Override
////        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////            switch (i){
////                case 0: i =1;
////                    break;
////                case 1: i =2;
////                    break;
////                case 2: i =3;
////                    break;
////                case 3: i =4;
////                    break;
////                case 4: i =5;
////                    break;
////                default:i=0;
////
////            }
////        }
////    });
//
//
////    data.add(new StudentCourseSelction(value,value2,value3));
////    Toast.makeText(getApplicationContext(), "New Course Is Added", Toast.LENGTH_LONG).show();
////    list.setAdapter(adapter);
////    list2.setAdapter(adapter2);
////    list3.setAdapter(adapter3);
//
//
//
//
//
//
//    }
////


}

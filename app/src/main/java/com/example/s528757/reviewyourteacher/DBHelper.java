package com.example.s528757.reviewyourteacher;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by S528758 on 12/3/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "courses";
    public static final String COLUMN_COURSE= "course";
    public static final String COLUMN_PROFESSOR= "professor";
    public static final String COLUMN_SECTION= "section";



    public DBHelper(Context context, Integer x) {

        super(context, "coursesDB", null, 1);

    }


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, "coursesDB", factory, version);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, "coursesDB", factory, version, errorHandler);
    }

    private static final String createDB = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_COURSE + " TEXT, " + COLUMN_PROFESSOR + " TEXT, " + COLUMN_SECTION + " TEXT, " + ");";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL(createDB);

        } catch (Exception e){
            Log.d("coursesDB",e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("Drop table if exists courses");

        onCreate(sqLiteDatabase);
    }
}

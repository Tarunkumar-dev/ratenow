package com.example.s528757.reviewyourteacher;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by S528757 on 10/27/2017.
 */

public class StudentCourseSelction extends AppCompatActivity {

    private String CourseName;
    private String ProfName;
    private String SecNumber;

//    public void Review()
//    {
//        Log.v("Review", "did something");
//    }


    public StudentCourseSelction(String courseName, String profName, String secNumber) {
        CourseName = courseName;
        ProfName = profName;
        SecNumber = secNumber;
    }
    public StudentCourseSelction(String courseName)
    {
        CourseName = courseName;
    }

    public void setCourseNameName(String name) {
        CourseName = name;
    }

    public void setProfNameName(String name2) {
        ProfName = name2;
    }
    public void setSecNumber(String sec) {
        SecNumber = sec;
    }


    public String getCourseName() {
        return CourseName;
    }

    public String getProfName() {
        return ProfName;
    }

    public String getSecNumber() {
        return SecNumber;
    }

    @Override
    public String toString() {
        return "StudentCourseSelection{" +
                "CourseName='" + CourseName + '\'' +
                ", ProfName='" + ProfName + '\'' +
                ", SecNumber=" + SecNumber +
                '}';
    }












}
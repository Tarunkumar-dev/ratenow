package com.example.s528757.reviewyourteacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class StudentReviewActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
                startActivity(intent);
//                Toast.makeText(StudentReviewActivity.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), studentCourses.class);
                startActivity(intent1);
//                Toast.makeText(StudentReviewActivity.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
//                Toast.makeText(StudentReviewActivity.this,"logged out",Toast.LENGTH_LONG).show();

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_review);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);
        Button btn, btn2;
        btn = (Button) findViewById(R.id.reviewst);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StudentReviewQs.class);
                startActivity(intent);
            }
        });

        btn2= (Button) findViewById(R.id.reviewstt);

        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StudentReviewQs.class);
                startActivity(intent);
            }
        });
    }
//    public void review() {
//        Intent i = new Intent(getApplicationContext(), StudentReviewQs.class);
//        startActivity(i);
//    }
//
//    public void review2() {
//        Intent i = new Intent(getApplicationContext(), StudentReviewQs.class);
//        startActivity(i);
//    }

}
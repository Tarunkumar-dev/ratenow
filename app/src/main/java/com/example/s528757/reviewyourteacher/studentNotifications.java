package com.example.s528757.reviewyourteacher;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.backendless.Backendless;
import com.backendless.IDataStore;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class studentNotifications extends AppCompatActivity {

//String [] notifications =  new String[]{};

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);

//        final LinearLayout MainLL = (LinearLayout)findViewById(R.id.list);
//        final TextView text = new TextView(this);
//
//        final IDataStore<Notification> notification = (IDataStore<Notification>) Backendless.Data.of(Notification.class).find();
//
//        DataQueryBuilder queryBuilder = DataQueryBuilder.create();
//        queryBuilder.getProperties();
//        queryBuilder.setSortBy("description");
//
//        notification.find(queryBuilder, new AsyncCallback<List<Notification>>() {
//            @Override
//            public void handleResponse(List<Notification> response) {
//                for(int i=0; i<response.size();i++){
//                notifications[i] = response.get(i).toString();
//
//                    text.setText(notifications[i]);
//                    text.setTextSize(16);
//                    text.setGravity(Gravity.LEFT);
//                    text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//                    MainLL.addView(text);
//                }
//                Log.d("Notifications", "Retrieved"+response.size()+"objects");
//              for(int i=0; i<response.size();i++){
//                   Log.d("Notifications", "Retrieved"+response.get(i)+"object");
//              }
//        }
//
//            @Override
//            public void handleFault(BackendlessFault fault) {
//
//            }
//        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);
        return super.onCreateOptionsMenu(menu);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
                startActivity(intent);
                Toast.makeText(studentNotifications.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), studentCourses.class);
                startActivity(intent1);
                Toast.makeText(studentNotifications.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                Toast.makeText(studentNotifications.this,"logged out",Toast.LENGTH_LONG).show();

        }
        return super.onOptionsItemSelected(item);
    }


}

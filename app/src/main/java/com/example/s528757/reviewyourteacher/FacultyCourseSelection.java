package com.example.s528757.reviewyourteacher;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by S528757 on 10/27/2017.
 */

public class FacultyCourseSelection extends AppCompatActivity {

    private String CourseName;
//    private String ProfName;
    private String SecNumber;

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getSecNumber() {
        return SecNumber;
    }

    public void setSecNumber(String secNumber) {
        SecNumber = secNumber;
    }

    public FacultyCourseSelection(String courseName, String secNumber) {
        CourseName = courseName;
        SecNumber = secNumber;
    }

    @Override
    public String toString() {
        return "FacultyCourseSelction{" +
                "CourseName='" + CourseName + '\'' +
                ", SecNumber='" + SecNumber + '\'' +
                '}';
    }
}


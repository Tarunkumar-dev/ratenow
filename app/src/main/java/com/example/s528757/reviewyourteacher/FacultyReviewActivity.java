package com.example.s528757.reviewyourteacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class FacultyReviewActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), facultyNotifications.class);
                startActivity(intent);
//                Toast.makeText(FacultyReviewActivity.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), facultyCourses.class);
                startActivity(intent1);
//                Toast.makeText(FacultyReviewActivity.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
//                Toast.makeText(FacultyReviewActivity.this,"logged out",Toast.LENGTH_LONG).show();
return true;
        }
        return super.onOptionsItemSelected(item);
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_review);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);
        Button btn, btn2;
        btn = (Button) findViewById(R.id.freviewst);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FacultyReviewQs.class);
                startActivity(intent);
            }
        });

        btn2= (Button) findViewById(R.id.freviewstt);

        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FacultyReviewQs.class);
                startActivity(intent);
            }
        });
    }
}

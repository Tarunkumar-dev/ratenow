package com.example.s528757.reviewyourteacher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StudentReviewQs extends AppCompatActivity {


    TextView tv,tv2,tv3;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), studentNotifications.class);
                startActivity(intent);
//                Toast.makeText(StudentReviewQs.this,"",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), studentCourses.class);
                startActivity(intent1);
//                Toast.makeText(StudentReviewQs.this,"",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
//                Toast.makeText(StudentReviewQs.this,"logged out",Toast.LENGTH_LONG).show();

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_review_qs);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);
        Button btn;
tv=(TextView)findViewById(R.id.Q1) ;
        tv2=(TextView)findViewById(R.id.Q2) ;
        tv3=(TextView)findViewById(R.id.Comments) ;
        btn= (Button) findViewById(R.id.submitR);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StudentReviewActivity.class);
                Toast.makeText(getApplicationContext(), "Your response has been recorded", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

}
//public void T(View view)
//{
//    Toast.makeText(getApplicationContext(), "Your response has been recorded", Toast.LENGTH_LONG).show();
//}
    public void saveInfo(View view)
    {
        SharedPreferences sharedPref=getSharedPreferences("qdetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =sharedPref.edit();
        editor.putString("Q1",tv.getText().toString());
        editor.putString("Q2",tv2.getText().toString());
        editor.putString("comments",tv3.getText().toString());
editor.apply();
    }


}
//    View v;
//    TextView tv1 = v.findViewById(R.id.Android);
//    TextView tv2= v.findViewById(R.id.Hoot);
//TextView tv3=v.findViewById(R.id.Cname);
//    TextView tv4=v.findViewById(R.id.pname);
//         tv3=tv1;


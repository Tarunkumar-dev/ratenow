package com.example.s528757.reviewyourteacher;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class facultyCourses extends AppCompatActivity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), facultyNotifications.class);
                startActivity(intent);
                Toast.makeText(facultyCourses.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), facultyCourses.class);
                startActivity(intent1);
                Toast.makeText(facultyCourses.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                Toast.makeText(facultyCourses.this,"logged out",Toast.LENGTH_LONG).show();

        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<FacultyCourseSelection> data = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_courses);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);

        final ArrayAdapter<FacultyCourseSelection> adapter = new FacultyCoursesLVAdapter(this, R.layout.faculty_course_selection, R.id.Course, data);
        final ListView list = (ListView) findViewById(R.id.sCourseLV);
        list.setAdapter(adapter);

    }




    public void submit(View v)
    {
        Intent intent = new Intent(getApplicationContext(), FacultyReviewActivity.class);
        startActivity(intent);
    }


    public void newCourse(View v)
    {
        final ArrayAdapter<FacultyCourseSelection> adapter = new FacultyCoursesLVAdapter(this, R.layout.faculty_course_selection, R.id.Course, data);
        final ListView list = (ListView) findViewById(R.id.sCourseLV);
        EditText et = (EditText) findViewById(R.id.ETCourse);
        String value = et.getText().toString();


        final ArrayAdapter<FacultyCourseSelection> adapter3 = new FacultyCoursesLVAdapter(this, R.layout.student_course_selction, R.id.Section, data);
        final ListView list3 = (ListView) findViewById(R.id.sCourseLV);
        EditText et3 = (EditText) findViewById(R.id.ETSec);
        String value3 = et3.getText().toString();




        data.add(new FacultyCourseSelection(value,value3));
        Toast.makeText(getApplicationContext(), "New Course Is Added", Toast.LENGTH_LONG).show();
        list.setAdapter(adapter);
        list3.setAdapter(adapter3);


    }
}

package com.example.s528757.reviewyourteacher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FacultyReviewQs extends AppCompatActivity {
    TextView tv,tv2,tv3,tv4;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.notifications:
                Intent intent = new Intent(getApplicationContext(), facultyNotifications.class);
                startActivity(intent);
//                Toast.makeText(FacultyReviewActivity.this,"Hello student",Toast.LENGTH_LONG).show();
                return true;
            case R.id.courses:
                Intent intent1 = new Intent(getApplicationContext(), facultyCourses.class);
                startActivity(intent1);
//                Toast.makeText(FacultyReviewActivity.this,"Select your courses",Toast.LENGTH_LONG).show();
                return true;
            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), Login.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
//                Toast.makeText(FacultyReviewActivity.this,"logged out",Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_review_qs);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.rats);

        Button btn;
        tv=(TextView)findViewById(R.id.FQ1) ;
        tv2=(TextView)findViewById(R.id.FQ2) ;
        tv3=(TextView)findViewById(R.id.FComments) ;
        tv4=(TextView)findViewById(R.id.avgrate) ;
tv.setText("5");
        tv2.setText("8");
        tv3.setText("Could be better with examples");
        tv4.setText("6.5");
        btn= (Button) findViewById(R.id.submitRR);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FacultyReviewActivity.class);
                startActivity(intent);
            }
        });
    }


//Code to display shared preferences
    public void displayData(View view)
    {
        SharedPreferences sharedPref= getSharedPreferences("qdetails", Context.MODE_PRIVATE);
        String question= sharedPref.getString("Q1"," ");
        String question2= sharedPref.getString("Q2"," ");
        String comments= sharedPref.getString("Comments"," ");
        tv.setText(question);
        tv2.setText(question2);
        tv3.setText(comments);
    }


}

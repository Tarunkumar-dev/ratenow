package com.example.s528757.reviewyourteacher;

import android.graphics.Movie;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.HashMap;
import java.util.Map;

public class AdminNotifications extends AppCompatActivity {
    Button btn;
    EditText notification;
    HashMap notifictn = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_notifications);
        btn = (Button) findViewById(R.id.notif);
        notification = (EditText) findViewById(R.id.notificationText);
        //final Notification notify = new Notification();

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // notify.description = notification.getText().toString();
                notifictn.put("description", notification.getText().toString());
//                Backendless.Data.of(Notification.class).save(notify, new AsyncCallback<Notification>() {
//
//                    @Override
//                    public void handleResponse(Notification response) {
//
//                        Toast.makeText(getApplicationContext(), "Notification sent to all users", Toast.LENGTH_SHORT).show();
//                        notification.setText("");
//                    }
//
//                    @Override
//                    public void handleFault(BackendlessFault fault) {
//                        Toast.makeText(getApplicationContext(), "Notification sent to all users", Toast.LENGTH_SHORT).show();
//                    }
//                });

                Backendless.Persistence.of("Notification").save(notifictn, new AsyncCallback<Map>() {
                    @Override
                    public void handleResponse(Map response) {
                        Toast.makeText(getApplicationContext(), "Notification sent to all users", Toast.LENGTH_SHORT).show();
                        notification.setText("");
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {

                    }
//                    public void handleResponse( Map response )
//                    {
//
//                        Toast.makeText(getApplicationContext(),"Registration Successfull", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(getApplicationContext(), Login.class);
//                        startActivity(intent);
//                        Log.d("Signup succesfull : ","Signup succesfull data is sent to backendless DB ");
//                    }
//
//                    public void handleFault( BackendlessFault fault )
//                    {
//                        Toast.makeText(getApplicationContext(), "Registration Unsuccessful", Toast.LENGTH_LONG).show();
//
//                    }
//                });

                });
            }
        });
    }
}
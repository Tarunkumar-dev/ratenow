# Rate Now

A Professor rating system that is built for android users using

- Android Studion
- Backendless


# Team Members:

1.Tarun Kumar Potlapalli

2.Nagnath Prabhu

3.Balakrishna vijaygiri

4.Koushik Lakkaraju


# Risk Components:

1.	Mobile backend as a service(MBaas) for DB ( Backendless )
2.	Tabbed activities for student and faculty modules

# Project Walkthrough

Admin can login with id:Admin pw:Admin ( these credentials are generated in backendless DB) 

Also a user can signup using the signup view and the details are stored in the backendless DB.

Project starts with a login page. We have 3 modules for student, faculty, admin.
For student use I/D: tarun 
P/W: tarun

For faculty use I/D: faculty 
P/W: password

For admin use I/D: ngp 
P/W: ngp

Click the Signup link to go to signup view

Enter Name, Email and password to signup


# Objects in the persistent storage:



Login page: User ID and PW require DB services

Registration page: details like email, pw need Db services.

Notifications page : This needs to have a separate DB space and Push notification Api

Student courses details: List items need to be saved in DB

Review questions page: Needs to have a separate table in the DB controlled by Faculty

Faculty courses: List items need to be saved in DB

# Testing Device

Google Nexus 5 � 4.4.4 � API 19 � 1080x1920